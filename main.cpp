#include "path_algoritms.cpp"
#include <functional>
#include <mutex>
#include <future>

std::mutex GLOBAL_MUTEX;

template<typename T, typename priority_t>
struct myPriorityQueue: public PriorityQueue<T, priority_t> {
    //returns a set of priority values for parallelisation
    std::vector<T> get_kit(){
        T best_item, other_one;
        priority_t best_priority, other_priority;
        std::vector<T> best_items;
        best_item = PriorityQueue<T, priority_t>::elements.top().second;
        best_priority = PriorityQueue<T, priority_t>::elements.top().first;
        best_items.push_back(best_item);
        PriorityQueue<T, priority_t>::elements.pop();
        //fill vector until best items exist
        while(!PriorityQueue<T, priority_t>::elements.empty()){
            other_one = PriorityQueue<T, priority_t>::elements.top().second;
            other_priority = PriorityQueue<T, priority_t>::elements.top().first;
            if(std::abs(other_priority - best_priority) > __DBL_EPSILON__)
                break;
            best_items.push_back(other_one);
            PriorityQueue<T, priority_t>::elements.pop();
        }
        return best_items;
    }

    //merge two queues
    void put(myPriorityQueue<T,priority_t> elem){
        while(!elem.empty()){
            auto value = elem.elements.top();
            PriorityQueue<T, priority_t>::elements.push(value);
            elem.elements.pop();
        }
    }

    void put(T item, priority_t priority){
        PriorityQueue<T, priority_t>::put(item, priority);
    }

};

//common case
template<typename Graph>
myPriorityQueue< typename Graph::Location, double> a_star_func(
        const Graph& graph,
        typename Graph::Location current,
        typename Graph::Location goal,
        std::unordered_map<typename Graph::Location, typename Graph::Location>& came_from,
        std::unordered_map<typename Graph::Location, double>& cost_so_far
        )
{
    myPriorityQueue<typename Graph::Location, double> result;

    for (auto& next : graph.neighbors(current)) {
      double new_cost = cost_so_far[current] + graph.cost(current, next);
      if (!cost_so_far.count(next) || new_cost < cost_so_far[next]) {
        double priority = new_cost + heuristic(next, goal);
        result.put(next, priority);
        //TODO: there is better case
        GLOBAL_MUTEX.lock();
        cost_so_far[next] = new_cost;
        came_from[next] = current;
        GLOBAL_MUTEX.unlock();
      }
    }

    return result;
}

//local case
myPriorityQueue< SquareGrid::Location, double> a_star_func_local(
        GridWithWeights& graph,
        SquareGrid::Location current,
        SquareGrid::Location goal,
        std::unordered_map<SquareGrid::Location, SquareGrid::Location>& came_from,
        std::unordered_map<SquareGrid::Location, double>& cost_so_far
        )
{
    myPriorityQueue<typename SquareGrid::Location, double> result;

    for (auto& next : graph.neighbors(current)) {
      double new_cost = cost_so_far[current] + graph.cost(current, next);
      if (!cost_so_far.count(next) || new_cost < cost_so_far[next]) {
        double priority = new_cost + heuristic(next, goal);
        result.put(next, priority);
        //TODO: there is better case
        GLOBAL_MUTEX.lock();
        cost_so_far[next] = new_cost;
        came_from[next] = current;
        GLOBAL_MUTEX.unlock();
      }
    }

    return result;
}


template<typename Graph>
void my_a_star_search
  (const Graph& graph,
   typename Graph::Location start,
   typename Graph::Location goal,
   std::unordered_map<typename Graph::Location, typename Graph::Location>& came_from,
   std::unordered_map<typename Graph::Location, double>& cost_so_far,
   //this case allows to use functor, lambda functions, or other functions
   //for algorithms you want. It's "a star" in our case
   std::function<
   myPriorityQueue< typename Graph::Location, double> /*return value*/
   //arguments
   (const Graph&,
   typename Graph::Location,
   typename Graph::Location,
   std::unordered_map<typename Graph::Location, typename Graph::Location>&,
   std::unordered_map<typename Graph::Location, double>&)
   >
   search_algoritm)
{



  typedef typename Graph::Location Location;
  myPriorityQueue<Location, double> frontier;

  frontier.put(start, 0);

  came_from[start] = start;
  cost_so_far[start] = 0;

  auto isgoal = [](std::vector<Location>& front_values, Location goal) -> bool
  {
      bool isvalues = false;
      for(auto& current: front_values)
          if(current == goal)
              return isvalues = true;
      return isvalues;
  };

  while (!frontier.empty()) {
    auto front_values = frontier.get_kit();

    if (isgoal(front_values, goal)) {
      break;
    }

    //one thread case
    //std::vector< myPriorityQueue<Location, double> > new_front;
    //for(auto val: front_values)
    //   new_front.push_back(search_algoritm(graph, val, goal, came_from, cost_so_far));

    //parallelisation case
    std::vector< std::future< myPriorityQueue<Location, double> > > new_front;
    for(auto val: front_values){
       auto elem = std::async(search_algoritm,std::ref(graph), std::move(val), std::move(goal), std::ref(came_from), std::ref(cost_so_far));
       new_front.push_back(std::move(elem));
    }

    for(auto& val: new_front)
        //frontier.put(val); //one thread
        frontier.put(val.get()); //parallelisation case

  }
}


int main() {
  GridWithWeights grid = make_diagram4();
  SquareGrid::Location start{1, 4};
  SquareGrid::Location goal{8, 5};
  std::unordered_map<SquareGrid::Location, SquareGrid::Location> came_from;
  std::unordered_map<SquareGrid::Location, double> cost_so_far;
  my_a_star_search<GridWithWeights>(grid, start, goal, came_from, cost_so_far, a_star_func<GridWithWeights>/*this gets code universal*/);
  draw_grid(grid,nullptr, &came_from);
  std::cout << std::endl;
  draw_grid(grid, &cost_so_far, nullptr);
  std::cout << std::endl;
  std::vector<SquareGrid::Location> path = reconstruct_path(start, goal, came_from);
  draw_grid(grid,  nullptr, nullptr, &path);
}
